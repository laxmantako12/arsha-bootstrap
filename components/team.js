import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import team1 from '../public/assets/team/team-1.jpg';
import team2 from '../public/assets/team/team-2.jpg';
import team3 from '../public/assets/team/team-3.jpg';
import team4 from '../public/assets/team/team-4.jpg';
import styles from '../styles/sass/team.module.scss'
import {
  Twitter
} from 'react-bootstrap-icons';

import {
  ImFacebook
} from "react-icons/im";

import { BsLinkedin } from "react-icons/bs";
import { AiFillInstagram } from "react-icons/ai";


const team = () => {
  return (
    
    <section id="team" className={`${styles.team} section-bg`}>
      <div className="container" data-aos="fade-up">

        <div className="section-title">
          <h2>Team</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div className="row">

          <div className="col-lg-6">
            <div className={`${styles.member} d-flex align-items-start`} data-aos="zoom-in" data-aos-delay="100">
              <div className={styles.pic}>
                <Image src={team1} />

              </div>
              <div className={styles.memberInfo}>
                <h4>Walter White</h4>
                <span>Chief Executive Officer</span>
                <p>Explicabo voluptatem mollitia et repellat qui dolorum quasi</p>
                <div className={styles.social}>
                  <Link href="#"><a ><Twitter color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><ImFacebook color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><AiFillInstagram color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><BsLinkedin color='#37517e' size={16} /> </a></Link>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 mt-4 mt-lg-0">
            <div className={`${styles.member} d-flex align-items-start`} data-aos="zoom-in" data-aos-delay="200">
              <div className={styles.pic}>
                <Image src={team2} />
              </div>
              <div className={styles.memberInfo}>
                <h4>Sarah Jhonson</h4>
                <span>Product Manager</span>
                <p>Aut maiores voluptates amet et quis praesentium qui senda para</p>
                <div className={styles.social}>
                  <Link href="#"><a ><Twitter color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><ImFacebook color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><AiFillInstagram color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><BsLinkedin color='#37517e' size={16} /> </a></Link>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 mt-4">
            <div className={`${styles.member} d-flex align-items-start`} data-aos="zoom-in" data-aos-delay="300">
              <div className={styles.pic}>
                <Image src={team3} />
              </div>
              <div className={styles.memberInfo}>
                <h4>William Anderson</h4>
                <span>CTO</span>
                <p>Quisquam facilis cum velit laborum corrupti fuga rerum quia</p>
                <div className={styles.social}>
                  <Link href="#"><a ><Twitter color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><ImFacebook color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><AiFillInstagram color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><BsLinkedin color='#37517e' size={16} /> </a></Link>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 mt-4">
            <div className={`${styles.member} d-flex align-items-start`} data-aos="zoom-in" data-aos-delay="400">
              <div className={styles.pic}>
                <Image src={team4} />
              </div>
              <div className={styles.memberInfo}>
                <h4>Amanda Jepson</h4>
                <span>Accountant</span>
                <p>Dolorum tempora officiis odit laborum officiis et et accusamus</p>
                <div className={styles.social}>
                  <Link href="#"><a ><Twitter color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><ImFacebook color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><AiFillInstagram color='#37517e' size={16} /></a></Link>
                  <Link href="#"><a href=""><BsLinkedin color='#37517e' size={16} /> </a></Link>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>
  )
}

export default team
