import React from 'react';
import Accordion from 'react-bootstrap/Accordion';
import styles from '../styles/sass/faq.module.scss';
import { BiHelpCircle } from "react-icons/bi";

const faq = () => {
  return (
    // <section id="faq" className="faq section-bg">
    <section id="faq" className={`${styles.faq} section-bg`}>
      <div className="container" data-aos="fade-up">

        <div className="section-title">
          <h2>Frequently Asked Questions</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div className={styles.faq__List}>
          <Accordion defaultActiveKey="0">
            <Accordion.Item eventKey="0" data-aos="fade-up" data-aos-delay="100">
              <Accordion.Header> <BiHelpCircle color='#47b2e4' size={24} /> Feugiat scelerisque varius morbi enim nunc? </Accordion.Header>
              <Accordion.Body>
                <p>Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.</p>
              </Accordion.Body>
            </Accordion.Item>

            <Accordion.Item eventKey="1" data-aos="fade-up" data-aos-delay="200">
              <Accordion.Header><BiHelpCircle color='#47b2e4' size={24} /> Dolor sit amet consectetur adipiscing elit? </Accordion.Header>
              <Accordion.Body>
                <p> Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.</p>
              </Accordion.Body>
            </Accordion.Item>

            <Accordion.Item eventKey="3" data-aos="fade-up" data-aos-delay="300">
              <Accordion.Header><BiHelpCircle color='#47b2e4' size={24} /> Tempus quam pellentesque nec nam aliquam sem et tortor consequat? </Accordion.Header>
              <Accordion.Body>
                <p>Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis</p>
              </Accordion.Body>
            </Accordion.Item>

            <Accordion.Item eventKey="4" data-aos="fade-up" data-aos-delay="400">
              <Accordion.Header><BiHelpCircle color='#47b2e4' size={24} /> Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? </Accordion.Header>
              <Accordion.Body>
                <p>Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis</p>
              </Accordion.Body>
            </Accordion.Item>

            <Accordion.Item eventKey="5" data-aos="fade-up" data-aos-delay="500">
              <Accordion.Header><BiHelpCircle color='#47b2e4' size={24} /> Dolor sit amet consectetur adipiscing elit?</Accordion.Header>
              <Accordion.Body>
                <p>Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis</p>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>

      </div>
    </section>
  )
}

export default faq
