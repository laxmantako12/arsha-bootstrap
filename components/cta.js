import React from 'react';
import styles from '../styles/sass/cta.module.scss';

const bgImg = {
    // backgroundColor: '#283a5a',
    // backgroundImage: 'url("../assets/cta-bg.jpg")',
    // // backgroundSize: 'contain',
    // // backgroundPosition: 'right',
    // // backgroundRepeat: 'no-repeat'
}
const cta = () => {
  return (
      <section id="cta" className={styles.cta} style={bgImg}>
          <div className="container" data-aos="zoom-in">

              <div className="row">
                  <div className="col-lg-9 text-center text-lg-start">
                      <h3>Call To Action</h3>
                      <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
                  <div className="col-lg-3 cta-btn-container text-center">
                      <a className="cta-btn align-middle" href="#">Call To Action</a>
                  </div>
              </div>

          </div>
      </section>
  )
}

export default cta
