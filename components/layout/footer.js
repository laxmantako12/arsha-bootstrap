import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Form } from 'react-bootstrap'
import {
  Twitter,
  Instagram,
  Skype
} from 'react-bootstrap-icons';

import { BiChevronRight } from "react-icons/bi";
import { ImFacebook, ImLinkedin2 } from "react-icons/im";
import styles from '../../styles/sass/footer.module.scss';

const Footer = () => {
  return (
    <>
      <footer id={styles["footer"]}>
        <div className={styles.footerNewsletter}>
          <Container className='container'>
            <Row className='row justify-content-center'>
              <Col lg='6'>
                <h4>Join Our Newsletter</h4>
                <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                <Form action="" method="post">
                  <input type="email" name="email" />
                  <input type="submit" value="Subscribe" />
                </Form>
              </Col>
            </Row>
          </Container>
        </div>

        {/* end footer news letter  */}
        <div className={styles.footerTop}>
          <Container className="container">
            <Row className="row">
              <Col lg='3' md='6' className={styles.footerContact}>
                <h3>Arsha</h3>
                <p> A108 Adam Street <br /> New York, NY 535022<br /> United States <br /><br /> <strong>Phone:</strong> <Link href="callto:1 5589 55488 55">
                  <a>+1 5589 55488 55</a>
                </Link><br /> <strong>Email:</strong> <Link href="mailto:info@example.com"><a>info@example.com</a></Link><br /></p>
              </Col>

              <Col lg='3' md='6' className={styles.footerLinks}>
                <h4>Useful Links</h4>
                <ul>
                  <li><BiChevronRight color="#47b2e4" size={18} /> <a href="#">Home</a></li>
                  <li><BiChevronRight color="#47b2e4" size={18} /> <a href="#">About us</a></li>
                  <li><BiChevronRight color="#47b2e4" size={18} /> <a href="#">Services</a></li>
                  <li><BiChevronRight color="#47b2e4" size={18} /> <a href="#">Terms of service</a></li>
                  <li><BiChevronRight color="#47b2e4" size={18} /> <a href="#">Privacy policy</a></li>
                </ul>
              </Col>

              <Col lg='3' md='6' className={styles.footerLinks}>
                <h4>Our Services</h4>
                <ul>
                  <li>
                    <BiChevronRight color="#47b2e4" size={18} />
                    <Link href="#">
                      <a>Web Design</a>
                    </Link>
                  </li>
                  <li>
                    <BiChevronRight color="#47b2e4" size={18} />
                    <Link href="#">
                      <a>Web Development</a>
                    </Link>
                  </li>
                  <li>
                    <BiChevronRight color="#47b2e4" size={18} />
                    <Link href="#">
                      <a >Product Management</a>
                    </Link>
                  </li>
                  <li>
                    <BiChevronRight color="#47b2e4" size={18} />
                    <Link href="#">
                      <a>Marketing</a>
                    </Link>
                  </li>
                  <li>
                    <BiChevronRight color="#47b2e4" size={18} />
                    <Link href="#">
                      <a >Graphic Design</a>
                    </Link>
                  </li>
                </ul>
              </Col>

              <Col lg='3' md='6' className={styles.footerLinks}>
                <h4>Our Social Networks</h4>
                <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
                <div className={`${styles.socialLinks} mt-3`}>
                  <Link href="#">
                    <a className="twitter"><Twitter color="white" size={16} /></a>
                  </Link>
                  <Link href="#">
                    <a className="facebook"><ImFacebook color="white" size={16} /></a>
                  </Link>
                  <Link href="#">
                    <a className="instagram"><Instagram color="white" size={16} /></a>
                  </Link>
                  <Link href="#">
                    <a className="google-plus"><Skype color="white" size={16} /></a>
                  </Link>
                  <Link href="#">
                    <a className="dss"><ImLinkedin2 color="white" size={16} /></a>
                  </Link>

                </div>
              </Col>

            </Row>
          </Container>
        </div>
        {/* end footer top  */}

        <div className={styles.footerBottom}>
          <div className='container clearfix'>
            <div className={styles.copyright}>
              © Copyright <strong><span>Arsha</span></strong>. All Rights Reserved
            </div>
            <div className={styles.credits}> Designed by
              <Link href="https://bootstrapmade.com/">
                <a> BootstrapMade</a>
              </Link>
            </div>
          </div>
        </div>
        {/* end footer btm  */}
      </footer>
    </>
  )
}

export default Footer
