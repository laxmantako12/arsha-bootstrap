import React from 'react'
import { BsPlayCircle } from 'react-icons/bs';
import Image from 'next/image'
import heroImage from '../public/assets/hero-img.png';
import styles from '../styles/sass/hero.module.scss';
const hero = () => {
  return (
    <section id={styles["hero"]} className="d-flex align-items-center">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            <h1>Better Solutions For Your Business</h1>
            <h2>We are team of talented designers making websites with Bootstrap</h2>
            <div className="d-flex justify-content-center justify-content-lg-start">
              <a href="#about" className={`${styles.btnGetStarted} scrollto` }>Get Started</a>
              <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" className={`${styles.btnWatchVideo} glightbox `}>
                <BsPlayCircle size={32}  />
                <span>Watch Video</span></a>
            </div>
          </div>
          <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
            <Image className={styles.animated}
              src={heroImage}
              alt="Hero image"
              priority
            />
          </div>
        </div>
      </div>
    </section>
  )
}

export default hero
