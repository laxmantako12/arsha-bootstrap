import React from 'react';
import styles from '../styles/sass/pricing.module.scss';
import { BiCheck, BiX } from "react-icons/bi";

const pricing = () => {
  return (
    <section id="pricing" className={styles.pricing}>
      <div className="container" data-aos="fade-up">

        <div className="section-title">
          <h2>Pricing</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div className={`${styles.row} row`}>

          <div className="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div className={styles.box}>
              <h3>Free Plan</h3>
              <h4><sup>$</sup>0<span>per month</span></h4>
              <ul>
                <li><BiCheck color='#28a745;' size={24}/> Quam adipiscing vitae proin</li>
                <li><BiCheck color='#28a745;' size={24}/> Nec feugiat nisl pretium</li>
                <li><BiCheck color='#28a745;' size={24}/> Nulla at volutpat diam uteera</li>
                <li className={styles.na}><BiX color='#ccc;' size={24} /> <span>Pharetra massa massa ultricies</span></li>
                <li className={styles.na}><BiX color='#ccc;' size={24} /> <span>Massa ultricies mi quis hendrerit</span></li>
              </ul>
              <a href="#" className={styles.buyBtn}>Get Started</a>
            </div>
          </div>

          <div className="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div className={`${styles.featured} ${styles.box}`}>
              <h3>Business Plan</h3>
              <h4><sup>$</sup>29<span>per month</span></h4>
              <ul>
                <li><BiCheck color='#28a745;' size={24}/> Quam adipiscing vitae proin</li>
                <li><BiCheck color='#28a745;' size={24}/> Nec feugiat nisl pretium</li>
                <li><BiCheck color='#28a745;' size={24}/> Nulla at volutpat diam uteera</li>
                <li><BiCheck color='#28a745;' size={24}/> Pharetra massa massa ultricies</li>
                <li><BiCheck color='#28a745;' size={24}/> Massa ultricies mi quis hendrerit</li>
              </ul>
              <a href="#" className={styles.buyBtn}>Get Started</a>
            </div>
          </div>

          <div className="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div className={styles.box}>
              <h3>Developer Plan</h3>
              <h4><sup>$</sup>49<span>per month</span></h4>
              <ul>
                <li><BiCheck color='#28a745;' size={24}/> Quam adipiscing vitae proin</li>
                <li><BiCheck color='#28a745;' size={24}/> Nec feugiat nisl pretium</li>
                <li><BiCheck color='#28a745;' size={24}/> Nulla at volutpat diam uteera</li>
                <li><BiCheck color='#28a745;' size={24}/> Pharetra massa massa ultricies</li>
                <li><BiCheck color='#28a745;' size={24}/> Massa ultricies mi quis hendrerit</li>
              </ul>
              <a href="#" className={styles.buyBtn}>Get Started</a>
            </div>
          </div>

        </div>

      </div>
    </section>
  )
}

export default pricing
