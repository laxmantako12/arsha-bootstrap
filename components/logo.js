import React from 'react'
import Image from 'next/image'
import logo1 from '../public/assets/clients/client-1.png';
import logo2 from '../public/assets/clients/client-2.png';
import logo3 from '../public/assets/clients/client-3.png';
import logo4 from '../public/assets/clients/client-4.png';
import logo5 from '../public/assets/clients/client-5.png';
import logo6 from '../public/assets/clients/client-6.png';
import styles from '../styles/sass/logos.module.scss';

const logos = [
  {
    id: '1',
    src: logo1,
  },

  {
    id: '2',
    src: logo2,
  },

  {
    id: '3',
    src: logo3,
  },

  {
    id: '4',
    src: logo4,
  },

  {
    id: '5',
    src: logo5,
  },

  {
    id: '6',
    src: logo6,
  },
]
const logo = () => {
  return (
    <section id="clients" className={`${ styles.clients } section-bg`}>
      <div className="container">

        <div className="row" data-aos="zoom-in">
          {logos.map(logo => (
            <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
              <Image src={logo.src} />
            </div>
          ))}
        </div>

      </div>
    </section>
  )
}

export default logo
