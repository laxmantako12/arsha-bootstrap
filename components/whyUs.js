import React from 'react'
import Image from 'next/image'
import whyusImage from '../public/assets/why-us.png';
import Accordion from 'react-bootstrap/Accordion'
import styles from '../styles/sass/whyUs.module.scss';

const whyUs = () => {
    // console.log('dfhb');
    return (
        <section id="why-us" className={`${styles.whyUs} section-bg`}>
            <div className="container-fluid" data-aos="fade-up">

                <div className="row">

                    <div className="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                        <div className={styles.content}>
                            <h3>Eum ipsam laborum deleniti <strong>velit pariatur architecto aut nihil</strong></h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
                            </p>
                        </div>

                        <div className={styles.accordionlist}>
                            <Accordion defaultActiveKey="0">
                                <Accordion.Item eventKey="0">
                                    <Accordion.Header><span>01</span> Non consectetur a erat nam at lectus urna duis?</Accordion.Header>
                                    <Accordion.Body>
                                        <p>Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.</p>
                                    </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="1">
                                    <Accordion.Header><span>02</span> Feugiat scelerisque varius morbi enim nunc?</Accordion.Header>
                                    <Accordion.Body>
                                        <p> Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.</p>
                                    </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="3">
                                    <Accordion.Header><span>03</span> Dolor sit amet consectetur adipiscing elit?</Accordion.Header>
                                    <Accordion.Body>
                                        <p>Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis</p>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>

                    </div>

                    <div className="col-lg-5 align-items-stretch order-1 order-lg-2 img"  data-aos="zoom-in" data-aos-delay="150">
                        <Image src={whyusImage}/>
                    </div>
                </div>

            </div>
        </section>

    )
}

export default whyUs
