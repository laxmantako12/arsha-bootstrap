import React from 'react'
import Hero from '../components/hero';
import Logo from '../components/logo';
import About from '../components/about';
import WhyUs from '../components/whyUs';
import Skills from '../components/skills';
import Services from '../components/services';
import Cta from '../components/cta';
import Portfolio from '../components/portfolio';
import Team from '../components/team';
import Pricing from '../components/pricing';
import Faq from '../components/faq';
import Contact from '../components/contact';

export default function Home() {
  return (

    <React.Fragment>
      <Hero />
      <Logo />
      <About />
      <WhyUs />
      <Skills />
      <Services />
      <Cta />
      <Portfolio />
      <Team />
      <Pricing />
      <Faq />
      <Contact />
    </React.Fragment>
  )
}
