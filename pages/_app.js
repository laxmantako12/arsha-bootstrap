import React from 'react'
import { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/globals.css'
import '../styles/style.css'

import Layout from '../components/layout/layout'

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    import("bootstrap/dist/js/bootstrap");
    AOS.init();
    AOS.refresh();

    // if (typeof window === "undefined") {
    //   return;
    // }

    // import("../public/assets/main");

  }, [])
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}

export default MyApp
